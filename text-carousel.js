/**
 * @license
 * Copyright 2019 Google LLC
 * SPDX-License-Identifier: BSD-3-Clause
 */

import {LitElement, html, css, unsafeHTML} from 'https://cdn.jsdelivr.net/gh/lit/dist@2/all/lit-all.min.js';

/**
 * An example element.
 *
 * @fires count-changed - Indicates when the count changes
 * @slot - This element has a slot
 * @csspart button - The button
 */
export class MyElement extends LitElement {
  static get styles() {
    return css`
      @keyframes carousel {
        from {
          transform: translateX(0);
        }
        to {
          transform: translateX(250%);
        }
      }
      @keyframes carousel-reverse {
        from {
          transform: translateX(0);
        }
        to {
          transform: translateX(-250%);
        }
      }
      :host {
        width: 100vw;
        height: 94px;
        font-size: 47px;
        line-height: 94px;
        letter-spacing: 0.05em;
        white-space: nowrap;
        overflow-x: hidden;
      }
      :host .text-carousel-container {
        background-color: var(--bg);
        transition: background-color 0.1s;
      }
      :host .text-carousel-container:hover {
        background-color: var(--color);
      }
      :host .text-carousel-container p, :host .text-carousel-container a {
        color: var(--color);
      }
      :host .text-carousel-container p, :host .text-carousel-container:hover a {
        color: var(--bg);
      }
      :host .text-carousel {
        height: 94px;
        white-space: nowrap;
        vertical-align: middle;
        display: flex;
        align-items: center;
        justify-content: center;
      }
      :host .text-carousel.right {
        animation: carousel 32s linear infinite;
      }
      :host .text-carousel.left {
        animation: carousel-reverse 32s linear infinite;
      }
      :host img {
        height: 67px;
      }
      :host p {
        line-height: 94px;
        vertical-align: middle;
        white-space: nowrap;
        margin: 0 0.5em;
      }
      
      :host a {
        text-decoration: none !important;
      }
      
      @media screen and (min-width: 767px) {
        :host {
          width: 100vw;
          height: 200px;
          font-size: 100px;
          line-height: 200px;
        }
        :host .text-carousel {
          height: 144px;
          padding: 28px 0;
        }
        :host p {
          line-height: 200px;
        }
        
        :host img {
          height: 144px;
        }
      }
    `;
  }

  static get properties() {
    return {
      direction: String,
      bg: String,
      color: String,
    };
  }

  constructor() {
    super();

    if (!this.direction) {
      this.direction = 'right';
    }
  }

  render() {
    return html`
        <div class="text-carousel-container" style="--bg: ${this.bg};--color: ${this.color}">
            <div class="text-carousel ${this.direction}">
                ${this.carouselContent()}
            </div>
        </div>
    `;
  }

  carouselContent() {
    const content = unsafeHTML(this.innerHTML);
    return (html`${content}${content}${content}${content}${content}`);
  }
}

window.customElements.define('text-carousel', MyElement);
