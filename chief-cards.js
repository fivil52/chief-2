/**
 * @license
 * Copyright 2019 Google LLC
 * SPDX-License-Identifier: BSD-3-Clause
 */

import {LitElement, html, css, until} from 'https://cdn.jsdelivr.net/gh/lit/dist@2/all/lit-all.min.js';

/**
 * An example element.
 *
 * @fires count-changed - Indicates when the count changes
 * @slot - This element has a slot
 * @csspart button - The button
 */
export class ChiefCards extends LitElement {
  static get styles() {
    return css`
      @keyframes carousel {
        0% {
          transform: translateX(-100%);
        }
        100% {
          transform: translateX(-200%);
        }
      }
      @keyframes carousel-reverse {
        0% {
          transform: translateX(0);
        }
        100% {
          transform: translateX(100%);
        }
      }
      
      :host {
        --yellow: #FEE11A;
        --blue:#1D2B61;
      }
      
      @media screen and (max-width: 767px) {
      :host .card-container {
          display: flex;
          flex-direction: column;
          flex-wrap: wrap;
          gap: 40px;
          align-items: center;
          width: 100%;
        }

        :host .card-container > a:nth-child(n + 11) {
          display: none !important;
        }

        :host .card-container.card-container--reverse {
          display: none;
        }
      }
      
      @media screen and (min-width: 768px) {
        :host .card-container {
          display: flex;
          flex-direction: row;
          flex-wrap: nowrap;
          gap: 40px;
          animation: carousel 36s linear infinite;
          justify-content: flex-start;
        }
        :host .card-container.card-container--reverse {
          flex-direction: row-reverse;
          animation: carousel-reverse 36s linear infinite;
          margin-top: 24px;
          justify-content: flex-start;
        }
        :host .card-containers:hover .card-container,
        :host .card-containers:hover .card-container.card-container--reverse {
          animation-play-state: paused;
        }
      }
      
      :host .flip-card {
        background-color: transparent;
        width: 320px;
        height: 595px;
        perspective: 1000px; /* Remove this if you don't want the 3D effect */
      }

      /* This container is needed to position the front and back side */
      :host .flip-card-inner {
        position: relative;
        width: 100%;
        height: 100%;
        text-align: center;
        transition: transform 0.8s;
        transform-style: preserve-3d;
      }

      /* Do an horizontal flip when you move the mouse over the flip box container */
      :host .flip-card:hover .flip-card-inner {
        transform: rotateY(180deg);
      }

      /* Position the front and back side */
      :host .flip-card-front, .flip-card-back {
        position: absolute;
        width: 100%;
        height: 100%;
        -webkit-backface-visibility: hidden; /* Safari */
        backface-visibility: hidden;
      }
      
      :host .flip-card-front img, :host .flip-card-back img {
        width: 100%;
      }

      /* Style the front side (fallback if image is missing) */
      :host .flip-card-front {
        color: black;
      }

      /* Style the back side */
      :host .flip-card-back {
        background-color: var(--yellow);
        color: white;
        transform: rotateY(180deg);
      }
      
      :host svg {
        max-width: 100%;
        max-height: 100%;
      }
      
      :host .card-progress {
        position: absolute;
        left: 20px;
        bottom: 41px;
      }

    `;
  }

  static get properties() {
    return {
      /**
       * The name to say "Hello" to.
       * @type {string}
       */
      apiUrl: {type: String},
      chiefs: {state: true}
    };
  }

  constructor() {
    super();
    this.updateChiefs();
  }

  render() {
    return html`
        <div class="card-containers">
            <div class="card-container">
                ${until(this.chiefs, html`...`)}
            </div>
            <div class="card-container card-container--reverse">
                ${until(this.chiefs, html`...`)}
            </div>
        </div>
    `;
  }

  updateChiefs() {
    this.chiefs = fetch('https://captain-village.a-sociate.tw/wp-json/wp/v2/chief/').then((response) => response.json())
      .then((data) => {
        /* Array */
        data.sort(() => Math.random() - 0.5);
        let datas = data.concat(data);

        return html`
            ${datas.map((chief, index) => {
                return html`<a href="${chief.meta_box.donate_url}" target="_blank">
                <div class="flip-card">
                    <div class="flip-card-inner">
                        <div class="flip-card-front">
                            <img src="${this.getPhoto(chief.meta_box.card_front)}" alt="">
                            <svg class="card-progress" width="280" height="64" viewBox="0 0 280 64" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <rect y="24" width="280" height="40" fill="white"/>
                                <g clip-path="url(#clip0_311_16753)">
                                    <rect width="${this.progressBar(chief)}" height="37" transform="translate(0 27)" fill="#FEE11A"/>
                                    <g opacity="0.4">
                                        <line opacity="0.2" x1="-1202.25" y1="706.457" x2="1163.73" y2="-659.433" stroke="#1D2B61"/>
                                        <line opacity="0.2" x1="-1198.25" y1="713.495" x2="1167.73" y2="-652.505" stroke="#1D2B61"/>
                                        <line opacity="0.2" x1="-1194.25" y1="720.423" x2="1171.73" y2="-645.576" stroke="#1D2B61"/>
                                        <line opacity="0.2" x1="-1190.25" y1="727.352" x2="1175.73" y2="-638.648" stroke="#1D2B61"/>
                                        <line opacity="0.2" x1="-1186.25" y1="734.28" x2="1179.73" y2="-631.719" stroke="#1D2B61"/>
                                        <line opacity="0.2" x1="-1182.25" y1="741.209" x2="1183.73" y2="-624.791" stroke="#1D2B61"/>
                                        <line opacity="0.2" x1="-1178.25" y1="748.137" x2="1187.73" y2="-617.863" stroke="#1D2B61"/>
                                        <line opacity="0.2" x1="-1174.25" y1="755.065" x2="1191.73" y2="-610.935" stroke="#1D2B61"/>
                                        <line opacity="0.2" x1="-1170.25" y1="761.993" x2="1195.73" y2="-604.006" stroke="#1D2B61"/>
                                        <line opacity="0.2" x1="-1166.25" y1="768.921" x2="1199.73" y2="-597.078" stroke="#1D2B61"/>
                                        <line opacity="0.2" x1="-1162.25" y1="775.85" x2="1203.73" y2="-590.15" stroke="#1D2B61"/>
                                        <line opacity="0.2" x1="-1158.25" y1="782.778" x2="1207.73" y2="-583.221" stroke="#1D2B61"/>
                                        <line opacity="0.2" x1="-1154.25" y1="789.707" x2="1211.73" y2="-576.293" stroke="#1D2B61"/>
                                        <line opacity="0.2" x1="-1150.25" y1="796.635" x2="1215.73" y2="-459.365" stroke="#1D2B61"/>
                                        <line opacity="0.2" x1="-1146.25" y1="803.453" x2="1219.73" y2="-452.437" stroke="#1D2B61"/>
                                        <line opacity="0.2" x1="-1142.25" y1="810.491" x2="1223.73" y2="-555.508" stroke="#1D2B61"/>
                                        <line opacity="0.2" x1="-1138.25" y1="817.42" x2="1227.73" y2="-548.58" stroke="#1D2B61"/>
                                        <line opacity="0.2" x1="-1134.25" y1="824.348" x2="1231.73" y2="-541.651" stroke="#1D2B61"/>
                                        <line opacity="0.2" x1="-1130.25" y1="831.276" x2="1235.73" y2="-534.723" stroke="#1D2B61"/>
                                        <line opacity="0.2" x1="-1126.25" y1="838.205" x2="1239.73" y2="-527.795" stroke="#1D2B61"/>
                                        <line opacity="0.2" x1="-1122.25" y1="845.133" x2="1243.73" y2="-520.866" stroke="#1D2B61"/>
                                        <line opacity="0.2" x1="-1118.25" y1="852.061" x2="1247.73" y2="-513.939" stroke="#1D2B61"/>
                                        <line opacity="0.2" x1="-1114.25" y1="858.99" x2="1251.73" y2="-507.01" stroke="#1D2B61"/>
                                        <line opacity="0.2" x1="-1110.25" y1="865.918" x2="1255.73" y2="-500.082" stroke="#1D2B61"/>
                                        <line opacity="0.2" x1="-1106.25" y1="872.846" x2="1259.73" y2="-493.153" stroke="#1D2B61"/>
                                        <line opacity="0.2" x1="-1102.25" y1="879.775" x2="1263.73" y2="-486.225" stroke="#1D2B61"/>
                                        <line opacity="0.2" x1="-1098.25" y1="886.703" x2="1267.73" y2="-479.297" stroke="#1D2B61"/>
                                        <line opacity="0.2" x1="-1094.25" y1="893.631" x2="1271.73" y2="-472.368" stroke="#1D2B61"/>
                                        <line opacity="0.2" x1="-1090.25" y1="900.45" x2="1275.73" y2="-465.44" stroke="#1D2B61"/>
                                        <line opacity="0.2" x1="-1086.25" y1="907.488" x2="1279.73" y2="-458.512" stroke="#1D2B61"/>
                                        <line opacity="0.2" x1="-1082.25" y1="914.416" x2="1283.73" y2="-451.584" stroke="#1D2B61"/>
                                        <line opacity="0.2" x1="-1078.25" y1="921.344" x2="1287.73" y2="-444.655" stroke="#1D2B61"/>
                                        <line opacity="0.2" x1="-1074.25" y1="928.273" x2="1291.73" y2="-437.727" stroke="#1D2B61"/>
                                        <line opacity="0.2" x1="-1070.25" y1="935.201" x2="1295.73" y2="-430.798" stroke="#1D2B61"/>
                                        <line opacity="0.2" x1="-1066.25" y1="942.129" x2="1299.73" y2="-423.87" stroke="#1D2B61"/>
                                        <line opacity="0.2" x1="-1062.25" y1="949.058" x2="1303.73" y2="-416.942" stroke="#1D2B61"/>
                                        <line opacity="0.2" x1="-1058.25" y1="955.986" x2="1307.73" y2="-410.014" stroke="#1D2B61"/>
                                        <line opacity="0.2" x1="-1054.25" y1="962.914" x2="1311.73" y2="-403.086" stroke="#1D2B61"/>
                                        <line opacity="0.2" x1="-1050.25" y1="969.843" x2="1315.73" y2="-396.157" stroke="#1D2B61"/>
                                        <line opacity="0.2" x1="-1046.25" y1="976.771" x2="1319.73" y2="-389.229" stroke="#1D2B61"/>
                                        <line opacity="0.2" x1="-1042.25" y1="983.699" x2="1323.73" y2="-382.3" stroke="#1D2B61"/>
                                        <line opacity="0.2" x1="-1038.25" y1="990.628" x2="1327.73" y2="-375.372" stroke="#1D2B61"/>
                                        <line opacity="0.2" x1="-1034.25" y1="997.545" x2="1331.73" y2="-368.444" stroke="#1D2B61"/>
                                        <line opacity="0.2" x1="-1030.25" y1="1004.48" x2="1335.73" y2="-361.516" stroke="#1D2B61"/>
                                        <line opacity="0.2" x1="-1026.25" y1="1011.41" x2="1339.73" y2="-354.587" stroke="#1D2B61"/>
                                        <line opacity="0.2" x1="-1022.25" y1="1018.34" x2="1343.73" y2="-347.659" stroke="#1D2B61"/>
                                        <line opacity="0.2" x1="-1018.25" y1="1025.27" x2="1347.73" y2="-340.731" stroke="#1D2B61"/>
                                        <line opacity="0.2" x1="-1014.25" y1="1032.2" x2="1351.73" y2="-333.802" stroke="#1D2B61"/>
                                        <line opacity="0.2" x1="-1010.25" y1="1039.13" x2="1355.73" y2="-326.874" stroke="#1D2B61"/>
                                        <line opacity="0.2" x1="-1006.25" y1="1046.05" x2="1359.73" y2="-319.946" stroke="#1D2B61"/>
                                        <line opacity="0.2" x1="-1002.25" y1="1052.98" x2="1363.73" y2="-313.017" stroke="#1D2B61"/>
                                        <line opacity="0.2" x1="-998.247" y1="1059.91" x2="1367.73" y2="-306.089" stroke="#1D2B61"/>
                                        <line opacity="0.2" x1="-994.247" y1="1066.84" x2="1371.73" y2="-299.161" stroke="#1D2B61"/>
                                        <line opacity="0.2" x1="-990.247" y1="1073.77" x2="1375.73" y2="-292.233" stroke="#1D2B61"/>
                                        <line opacity="0.2" x1="-986.247" y1="1080.7" x2="1379.73" y2="-285.304" stroke="#1D2B61"/>
                                        <line opacity="0.2" x1="-982.247" y1="1087.62" x2="1383.73" y2="-278.376" stroke="#1D2B61"/>
                                        <line opacity="0.2" x1="-978.246" y1="1094.55" x2="1387.74" y2="-271.447" stroke="#1D2B61"/>
                                        <line opacity="0.2" x1="-974.246" y1="1101.48" x2="1391.74" y2="-264.519" stroke="#1D2B61"/>
                                        <line opacity="0.2" x1="-970.246" y1="1108.41" x2="1395.74" y2="-257.591" stroke="#1D2B61"/>
                                        <line opacity="0.2" x1="-966.246" y1="1115.34" x2="1399.74" y2="-250.663" stroke="#1D2B61"/>
                                        <line opacity="0.2" x1="-962.246" y1="1122.27" x2="1403.74" y2="-243.734" stroke="#1D2B61"/>
                                        <line opacity="0.2" x1="-958.246" y1="1129.19" x2="1407.74" y2="-236.806" stroke="#1D2B61"/>
                                        <line opacity="0.2" x1="-954.246" y1="1136.12" x2="1411.74" y2="-229.878" stroke="#1D2B61"/>
                                        <line opacity="0.2" x1="-950.246" y1="1143.05" x2="1415.74" y2="-222.949" stroke="#1D2B61"/>
                                        <line opacity="0.2" x1="-946.246" y1="1149.98" x2="1419.74" y2="-216.021" stroke="#1D2B61"/>
                                        <line opacity="0.2" x1="-942.246" y1="1145.91" x2="1423.74" y2="-209.093" stroke="#1D2B61"/>
                                        <line opacity="0.2" x1="-938.246" y1="1163.84" x2="1427.74" y2="-202.164" stroke="#1D2B61"/>
                                        <line opacity="0.2" x1="-934.246" y1="1170.76" x2="1431.74" y2="-195.236" stroke="#1D2B61"/>
                                        <line opacity="0.2" x1="-930.246" y1="1177.69" x2="1435.74" y2="-188.308" stroke="#1D2B61"/>
                                        <line opacity="0.2" x1="-926.246" y1="1184.62" x2="1439.74" y2="-181.38" stroke="#1D2B61"/>
                                        <line opacity="0.2" x1="-922.246" y1="1191.55" x2="1443.74" y2="-174.451" stroke="#1D2B61"/>
                                        <line opacity="0.2" x1="-918.246" y1="1198.48" x2="1447.74" y2="-167.523" stroke="#1D2B61"/>
                                        <line opacity="0.2" x1="-914.246" y1="1205.41" x2="1451.74" y2="-160.594" stroke="#1D2B61"/>
                                        <line opacity="0.2" x1="-910.246" y1="1212.33" x2="1455.74" y2="-153.666" stroke="#1D2B61"/>
                                        <line opacity="0.2" x1="-906.245" y1="1219.26" x2="1459.74" y2="-146.738" stroke="#1D2B61"/>
                                        <line opacity="0.2" x1="-902.245" y1="1226.19" x2="1463.74" y2="-139.81" stroke="#1D2B61"/>
                                        <line opacity="0.2" x1="-898.245" y1="1233.12" x2="1467.74" y2="-132.882" stroke="#1D2B61"/>
                                        <line opacity="0.2" x1="-894.245" y1="1240.05" x2="1471.74" y2="-125.953" stroke="#1D2B61"/>
                                        <line opacity="0.2" x1="-890.245" y1="1246.97" x2="1475.74" y2="-119.025" stroke="#1D2B61"/>
                                        <line opacity="0.2" x1="-886.245" y1="1253.9" x2="1479.74" y2="-112.096" stroke="#1D2B61"/>
                                        <line opacity="0.2" x1="-882.245" y1="1260.83" x2="1483.74" y2="-105.168" stroke="#1D2B61"/>
                                        <line opacity="0.2" x1="-878.245" y1="1267.76" x2="1487.74" y2="-98.2399" stroke="#1D2B61"/>
                                        <line opacity="0.2" x1="-874.245" y1="1274.69" x2="1491.74" y2="-91.3117" stroke="#1D2B61"/>
                                        <line opacity="0.2" x1="-870.245" y1="1281.62" x2="1495.74" y2="-84.383" stroke="#1D2B61"/>
                                        <line opacity="0.2" x1="-866.245" y1="1288.55" x2="1499.74" y2="-77.4547" stroke="#1D2B61"/>
                                        <line opacity="0.2" x1="-862.245" y1="1295.47" x2="1503.74" y2="-70.5265" stroke="#1D2B61"/>
                                        <line opacity="0.2" x1="-858.245" y1="1302.4" x2="1507.74" y2="-63.5983" stroke="#1D2B61"/>
                                        <line opacity="0.2" x1="-854.244" y1="1309.33" x2="1511.74" y2="-45.6696" stroke="#1D2B61"/>
                                        <line opacity="0.2" x1="-850.244" y1="1316.26" x2="1515.74" y2="-49.7414" stroke="#1D2B61"/>
                                        <line opacity="0.2" x1="-846.244" y1="1323.19" x2="1519.74" y2="-42.8136" stroke="#1D2B61"/>
                                        <line opacity="0.2" x1="-842.244" y1="1330.11" x2="1523.74" y2="-35.8854" stroke="#1D2B61"/>
                                        <line opacity="0.2" x1="-838.244" y1="1337.04" x2="1527.74" y2="-28.9457" stroke="#1D2B61"/>
                                        <line opacity="0.2" x1="-834.244" y1="1343.97" x2="1531.74" y2="-22.0285" stroke="#1D2B61"/>
                                        <line opacity="0.2" x1="-830.244" y1="1350.9" x2="1535.74" y2="-15.1002" stroke="#1D2B61"/>
                                        <line opacity="0.2" x1="-826.244" y1="1357.83" x2="1539.74" y2="-8.17203" stroke="#1D2B61"/>
                                        <line opacity="0.2" x1="-822.244" y1="1364.76" x2="1543.74" y2="-1.24332" stroke="#1D2B61"/>
                                        <line opacity="0.2" x1="-818.244" y1="1371.68" x2="1547.74" y2="5.68491" stroke="#1D2B61"/>
                                        <line opacity="0.2" x1="-814.244" y1="1378.61" x2="1551.74" y2="12.6131" stroke="#1D2B61"/>
                                        <line opacity="0.2" x1="-810.244" y1="1385.54" x2="1555.74" y2="19.5414" stroke="#1D2B61"/>
                                        <line opacity="0.2" x1="-806.244" y1="1392.47" x2="1559.74" y2="26.4701" stroke="#1D2B61"/>
                                        <line opacity="0.2" x1="-802.244" y1="1399.4" x2="1453.74" y2="33.3983" stroke="#1D2B61"/>
                                        <line opacity="0.2" x1="-798.244" y1="1406.33" x2="1457.74" y2="40.3265" stroke="#1D2B61"/>
                                        <line opacity="0.2" x1="-794.244" y1="1413.25" x2="1571.74" y2="47.2547" stroke="#1D2B61"/>
                                        <line opacity="0.2" x1="-790.244" y1="1420.18" x2="1575.74" y2="54.183" stroke="#1D2B61"/>
                                        <line opacity="0.2" x1="-786.244" y1="1427.11" x2="1579.74" y2="61.1112" stroke="#1D2B61"/>
                                    </g>
                                </g>
                                <text fill="#1D2B61" xml:space="preserve" style="white-space: pre" font-family="Noto Sans TC" font-size="16" font-weight="bold" letter-spacing="0.03em"><tspan x="93" y="50.476">&#x52df;&#x8cc7;&#x9032;&#x5ea6;</tspan></text>
                                <text fill="#1D2B61" xml:space="preserve" style="white-space: pre" font-family="Roboto" font-size="16" font-weight="800" letter-spacing="0.03em"><tspan x="159" y="49.9688">${Math.ceil(this.progressBarRatio(chief) * 100)}%</tspan></text>
                                <rect x="1.5" y="25.5" width="277" height="37" stroke="#1D2B61" stroke-width="3"/>
                                <rect width="135" height="24" fill="#1D2B61"/>
                                <text fill="#FEE11A" xml:space="preserve" style="white-space: pre" font-family="Noto Sans TC" font-size="12" font-weight="bold" letter-spacing="0.03em"><tspan x="8" y="17.232">&#x76ee;&#x6a19;&#x91d1;&#x984d; </tspan></text>
                                <text fill="#FEE11A" xml:space="preserve" style="white-space: pre" font-family="Roboto" font-size="12" font-weight="bold" letter-spacing="0.03em"><tspan x="60.5187" y="17.232">NT 150,000</tspan></text>
                                <defs>
                                    <clipPath id="clip0_311_16753">
                                        <rect width="280" height="37" fill="white" transform="translate(0 27)"/>
                                    </clipPath>
                                </defs>
                            </svg>
                        </div>
                        <div class="flip-card-back">
                            <img src="${this.getPhoto(chief.meta_box.card_back)}" alt="">
                        </div>
                    </div>
                </div>
            </a>`
            })}
        `;
      })
    ;
  }

  getPhoto(photo) {
    return photo[0] ? photo[0].full_url: '';
  }

  moneyFormat(target_amount) {
    let moneyString = String(target_amount)

    if (moneyString.length >= 7) {
      moneyString = `${moneyString.slice(0, moneyString.length - 6)},${moneyString.slice(3, moneyString.length - 3)},${moneyString.slice(6, moneyString.length)}`
    } else if (moneyString.length >= 4) {
      moneyString = `${moneyString.slice(0, moneyString.length - 3)},${moneyString.slice(3, moneyString.length)}`
    }

    return moneyString;
  }

  progressBarRatio(chief) {
    let n = (chief.meta_box.current_amount / chief.meta_box.target_amount);

    if (isNaN(n)) {
      return 0;
    } else {
      return n;
    }
  }

  progressBar(chief) {
    return String(280 * this.progressBarRatio(chief));
  }


}

window.customElements.define('chief-cards', ChiefCards);