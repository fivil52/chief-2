/**
 * @license
 * Copyright 2019 Google LLC
 * SPDX-License-Identifier: BSD-3-Clause
 */

import {LitElement, html, css, unsafeHTML} from 'https://cdn.jsdelivr.net/gh/lit/dist@2/all/lit-all.min.js';

/**
 * An example element.
 *
 * @fires count-changed - Indicates when the count changes
 * @slot - This element has a slot
 * @csspart button - The button
 */
export class MyElement extends LitElement {
  static get styles() {
    return css`
      @keyframes carousel {
        0% {
          transform: translateX(0);
        }
        50% {
          transform: translateX(100%);
        }
        100% {
          transform: translateX(0);
        }
      }
      @keyframes carousel-reverse {
        0% {
          transform: translateX(0);
        }
        50% {
          transform: translateX(-100%);
        }
        100% {
          transform: translateX(0);
        }
      }
      :host {
        width: 100vw;
        height: 26px;
        font-size: 20px;
        line-height: 56px;
        letter-spacing: 0.05em;
        white-space: nowrap;
        overflow-x: hidden;
      }
      @media screen and (min-width: 767px) {
        :host {
          height: 56px;
        }
      }
      :host .text-carousel {
        height: 56px;
        white-space: nowrap;
        vertical-align: middle;
        display: flex;
        align-items: center;
        justify-content: center;
        padding: 0;
      }
      :host .text-carousel.right {
        animation: carousel 64s linear infinite;
      }
      :host .text-carousel.left {
        animation: carousel-reverse 64s linear infinite;
      }
      :host p {
        line-height: 56px;
        vertical-align: middle;
        white-space: nowrap;
        margin: 0 2em;
      }
      :host img {
        height: 56px;
      }
      :host p {
        line-height: 56px;
        vertical-align: middle;
        white-space: nowrap;
        margin: 0 0.5em;
      }
    `;
  }

  static get properties() {
    return {
      direction: String,
      bg: String,
    };
  }

  constructor() {
    super();

    if (!this.direction) {
      this.direction = 'right';
    }
  }

  render() {
    return html`
        <div style="background-color: ${this.bg};">
            <div class="text-carousel ${this.direction}">
                ${this.carouselContent()}
            </div>
        </div>
    `;
  }

  carouselContent() {
    const content = unsafeHTML(this.innerHTML);
    return (html`${content}${content}${content}${content}${content}`);
  }
}

window.customElements.define('text-carousel-small', MyElement);
