/**
 * @license
 * Copyright 2019 Google LLC
 * SPDX-License-Identifier: BSD-3-Clause
 */

import {LitElement, html, css} from 'https://cdn.jsdelivr.net/gh/lit/dist@2/all/lit-all.min.js';

/**
 * An example element.
 *
 * @fires count-changed - Indicates when the count changes
 * @slot - This element has a slot
 * @csspart button - The button
 */
export class MyElement extends LitElement {
  static get styles() {
    return css`
      :host {
        --color-yellow: #FEE11A;
        --color-blue: #1D2B61;
        
        position: relative;
        display: block;
        width: 100%;
        border: 3px solid var(--color-blue);
        box-shadow: 4px 4px 0 #000000;
        background-color: #fff;
      }
      :host > div {
        background-image: var(--bg);
        background-size: auto 260px;
        background-position: top center;
        background-repeat: no-repeat;
        height: 357px;
      }
      :host .content {
        position: absolute;
        bottom: 0;
        margin: 12px;
        padding: 8px 15px;
        background: var(--color-blue);
        color:  var(--color-yellow);
        width: calc(100% - 54px);
      }
      :host .tag {
        box-sizing: border-box;
        font-size: 12px;
        line-height: 22px;
        background: #FEE11A;
        border: 2px solid #1D2B61;
        border-radius: 24px;
        color: var(--color-blue);
        text-align: center;
        padding: 0 27px;
        width: fit-content;
        min-width: 92px;
        font-weight: 700;
        margin-bottom: 8px;
      }
      :host .content {
        font-weight: 700;
        font-size: 18px;
        line-height: 30px;

        letter-spacing: 0.02em;

        /* #yellow */

        color: #FEE11A;
      }
    `;
  }

  static get properties() {
    return {
      /**
       * The name to say "Hello" to.
       * @type {string}
       */
      bg: {type: String},
    };
  }

  constructor() {
    super();
    this.bg = '';
  }

  render() {
    return html`
        <div style="--bg:url(${this.bg})">
            <div class="content">
                <div class="tag">
                    <slot name="card-tag"></slot>
                </div>
                <slot name="card-content" class="content"></slot>
            </div>
        </div>
    `;
  }
}

window.customElements.define('card-1', MyElement);
